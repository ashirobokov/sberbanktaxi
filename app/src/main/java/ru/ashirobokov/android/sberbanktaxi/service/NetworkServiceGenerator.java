package ru.ashirobokov.android.sberbanktaxi.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ashirobokov.android.sberbanktaxi.BuildConfig;

/**
 * Created by 1 on 27.02.2017.
 */
public class NetworkServiceGenerator {

    public static <S> S createNetworkService(String serviceUrl, Class<S> serviceClass) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    return retrofit.create(serviceClass);
    }

}
