package ru.ashirobokov.android.sberbanktaxi.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import ru.ashirobokov.android.sberbanktaxi.R;
import ru.ashirobokov.android.sberbanktaxi.presenter.MainPresenter;
import ru.ashirobokov.android.sberbanktaxi.presenter.MainPresenterImpl;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;
import ru.ashirobokov.android.sberbanktaxi.view.MainView;

public class MainActivity extends ProgressActivity implements MainView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "MainActivity";

    private Toolbar mToolBar;

    private CoordinatorLayout mCoordinator;
    private ImageView mInitMap;
    private ToggleButton mComfort;
    private ToggleButton mBusiness;
    private ToggleButton mVip;
    private Button mOrder;

    private MainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_main);

        mCoordinator = (CoordinatorLayout) findViewById(R.id.coordinator_main);

        mToolBar = (Toolbar) findViewById(R.id.app_toolbar);
        mToolBar.setTitle("Мой лимит " + mPresenter.getLimit() +" руб");
        setSupportActionBar(mToolBar);

        mInitMap = (ImageView) findViewById(R.id.map_location);
        mComfort = (ToggleButton) findViewById(R.id.toggle_comfort);
        mBusiness = (ToggleButton) findViewById(R.id.toggle_business);
        mVip = (ToggleButton) findViewById(R.id.toggle_vip);
        mOrder = (Button) findViewById(R.id.order_button);

        mPresenter = new MainPresenterImpl(this);

        mPresenter.showViewsInitial();

        final View.OnClickListener snackBarListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.requestForCarCategory();
            }

        };


        mOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    mPresenter.createOrder();
            }

        });

        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        switch (buttonView.getId()) {
                            case R.id.toggle_comfort:
                                 if (isChecked) {
                                       if (mPresenter.setupRequestCategory("comfort")){
                                           Snackbar snack = Snackbar.make(mCoordinator, "Запросить разрешение на комфорт?", Snackbar.LENGTH_INDEFINITE)
                                                   .setAction("Да", snackBarListener);
                                           snack.show();
                                       }
                                 }
                                break;
                            case R.id.toggle_business:
                                 if (isChecked) {
                                     if (mPresenter.setupRequestCategory("business")) {
                                         Snackbar snack = Snackbar.make(mCoordinator, "Запросить разрешение на бизнес?", Snackbar.LENGTH_INDEFINITE)
                                                 .setAction("Да", snackBarListener);
                                         snack.show();
                                     }
                                 }
                                break;
                            case R.id.toggle_vip:
                                if (isChecked) {
                                    if (mPresenter.setupRequestCategory("vip")) {
                                        Snackbar snack = Snackbar.make(mCoordinator, "Запросить разрешение на VIP?", Snackbar.LENGTH_INDEFINITE)
                                                .setAction("Да", snackBarListener);
                                        snack.show();
                                    }
                                }
                                break;
                        }
            }

        };


        mComfort.setOnCheckedChangeListener(listener);
        mBusiness.setOnCheckedChangeListener(listener);
        mVip.setOnCheckedChangeListener(listener);

    }

    @Override
    public void showInitialMapLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mInitMap.setImageDrawable(getDrawable(R.drawable.ordermap));
        } else {
            mInitMap.setImageResource(R.drawable.ordermap);
        }
    }

    @Override
    public void showToggleButtonChecked(String category) {

        if (category.equals("comfort"))
            mComfort.setChecked(true);
        if (category.equals("business"))
            mBusiness.setChecked(true);
        if (category.equals("vip"))
            mVip.setChecked(true);

    }

    @Override
    public void showOrderButtonActive(boolean enabled) {
        mOrder.setEnabled(enabled);
    }

    @Override
    public void showProgress() {
        super.showProgress();
    }

    @Override
    public void hideProgress() {
        super.hideProgress();
    }


    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


}