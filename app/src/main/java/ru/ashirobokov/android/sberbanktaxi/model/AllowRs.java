package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AShirobokov on 16.03.2017.
 */
public class AllowRs {

    @SerializedName("code")
    @Expose
    private int code;

    @SerializedName("allowed")
    @Expose
    private boolean allowed;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isAllowed() {
        return allowed;
    }

    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    @Override
    public String toString() {
        return "AllowRs{" +
                "code=" + code +
                ", allowed=" + allowed +
                '}';
    }

}
