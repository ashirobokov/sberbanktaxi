package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 1 on 09.03.2017.
 */
public class RideTimeResult {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("descr")
    @Expose
    private String descr;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RideTimeResult{" +
                "code=" + code +
                ", descr='" + descr + '\'' +
                ", data=" + data +
                '}';
    }

    public static class Data {


            @SerializedName("duration")
            @Expose
            private Integer duration;
            @SerializedName("dest_time")
            @Expose
            private Integer destTime;

            public Integer getDuration() {
                return duration;
            }

            public void setDuration(Integer duration) {
                this.duration = duration;
            }

            public Integer getDestTime() {
                return destTime;
            }

            public void setDestTime(Integer destTime) {
                this.destTime = destTime;
            }


            @Override
            public String toString() {
                return "Data{" +
                        "duration=" + duration +
                        ", destTime=" + destTime +
                        '}';
            }

        }

}