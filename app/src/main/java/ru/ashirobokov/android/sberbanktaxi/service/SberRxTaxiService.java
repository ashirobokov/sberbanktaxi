package ru.ashirobokov.android.sberbanktaxi.service;


import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.ashirobokov.android.sberbanktaxi.model.AllowRq;
import ru.ashirobokov.android.sberbanktaxi.model.AllowRs;
import rx.Observable;

/**
 * Created by 1 on 16.03.2017.
 */
public interface SberRxTaxiService {

    final String SBERTAXI_SERVICE = "http://localhost:9080/sbrf-taxi-service/";

//  http://localhost:9080/sbrf-taxi-service/allow

    @FormUrlEncoded
    @Headers( "Content-Type: application/json" )
    @POST("allow")
    Observable<AllowRs> getAllow(@Body AllowRq allowRq);

}
