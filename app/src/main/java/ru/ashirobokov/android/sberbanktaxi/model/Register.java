package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shirobokov on 15.02.2017.
 */
public class Register {

    @SerializedName("securityCode")
    @Expose
    private String securityCode;

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    @Override
    public String toString() {
        return "Register{" +
                "securityCode='" + securityCode + '\'' +
                '}';
    }

}