package ru.ashirobokov.android.sberbanktaxi.presenter;

import android.os.Handler;
import android.util.Log;

import ru.ashirobokov.android.sberbanktaxi.data.MainHelper;
import ru.ashirobokov.android.sberbanktaxi.model.AllowRq;
import ru.ashirobokov.android.sberbanktaxi.model.AllowRs;
import ru.ashirobokov.android.sberbanktaxi.model.Budget;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;
import ru.ashirobokov.android.sberbanktaxi.view.MainView;

/**
 * Created by AShirobokov on 21.03.2017.
 */
public class MainPresenterImpl implements MainPresenter {

    private static final String TAG = ConstantManager.TAG_PREFIX + "MainPresenter";

    private MainView mainView;
    private MainHelper mainHelper;

    private Budget budget;
    private String requested = new String();

    public MainPresenterImpl(MainView mainView) {
        this.mainView = mainView;
        this.mainHelper = new MainHelper();
        this.budget = mainHelper.getTestBudget();
    }


    @Override
    public int getLimit() {
        return budget.getRestOfLimit();
    }

    @Override
    public void showViewsInitial(){

        mainView.showInitialMapLocation();
        setupAllowedCarCategories();

    }

    @Override
    public boolean setupRequestCategory(String category) {

        if (category.equals("comfort"))
            if (!budget.getMyCarCategory().getComfort()){
                requested = "comfort";
                mainView.showOrderButtonActive(false);
                return true;
            }

        if (category.equals("business"))
            if (!budget.getMyCarCategory().getComfort()){
                requested = "business";
                mainView.showOrderButtonActive(false);
                return true;
            }

        if (category.equals("vip"))
            if (!budget.getMyCarCategory().getComfort()){
                requested = "vip";
                mainView.showOrderButtonActive(false);
                return true;
            }

    return false;
    }

    @Override
    public void requestForCarCategory() {

    new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
                AllowRq req = new AllowRq();
                req.setCarCategory(requested);
                AllowRs res = mainHelper.getTestCarCategoryAllow();

                Log.d(TAG, res.toString());

                if (res.isAllowed()) {
                    mainView.showMessage("Использование категории автомобиля РАЗРЕШЕНО!");
                    mainView.showOrderButtonActive(true);
                } else {
                    mainView.showMessage("Категория автомобиля НЕ РАЗРЕШЕНА для использования!");
                    setupAllowedCarCategories();
                }
            }
        }, 5000);

    }

    @Override
    public void createOrder() {

    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    private void setupAllowedCarCategories() {

        if (budget.getMyCarCategory().getComfort())
                mainView.showToggleButtonChecked("comfort");
        if (budget.getMyCarCategory().getBusiness())
                mainView.showToggleButtonChecked("business");
        if (budget.getMyCarCategory().getVip())
                mainView.showToggleButtonChecked("vip");
    }

}