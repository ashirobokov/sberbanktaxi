package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AShirobokov on 09.03.2017.
 */
public class OrderResult {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("descr")
    @Expose
    private String descr;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OrderResult{" +
                "code=" + code +
                ", descr='" + descr + '\'' +
                ", data=" + data +
                '}';
    }

    public static class Data {

        @SerializedName("order_id")
        @Expose
        private Integer orderId;

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "orderId=" + orderId +
                    '}';
        }

    }

}
