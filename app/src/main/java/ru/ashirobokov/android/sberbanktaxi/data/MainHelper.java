package ru.ashirobokov.android.sberbanktaxi.data;

import android.os.Handler;

import com.google.gson.Gson;

import ru.ashirobokov.android.sberbanktaxi.App;
import ru.ashirobokov.android.sberbanktaxi.model.AllowRs;
import ru.ashirobokov.android.sberbanktaxi.model.Budget;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;

/**
 * Created by Shirobokov on 12.03.2017.
 */

public class MainHelper {

    private static final String TAG = ConstantManager.TAG_PREFIX + "LoginHelper";
    private Gson gson;

    public MainHelper() {
        gson = App.get().getComponent().gson();
    }


    public Budget getTestBudget() {

        final String budgetStri = "{" +
        "\"firstName\" 	: \"Петр\"," + "\"middleName\"	: \"Петрович\"," + "\"lastName\"    : \"Петров\"," +
        "\"assignedLimit\"	: 104500," + "\"distributedLimit\"	: 80000," + "\"restOfLimit\"	: 24500," +
        "\"myCarCategory\" 	: {" + "\"comfort\" : true," + "\"business\" : true," + "\"vip\" : true" + "}," +
        "\"colleagues\": 	[" +
            "{" + "\"firstName\"    : \"Дарья\"," + "\"lastName\"  	: \"Палеева\"," +
                "\"carCategory\" 	: {" + "\"comfort\" : true," + "\"business\" : true," + "\"vip\" : true" + "}," +
                "\"limit\"    	: 5000," + "\"distribution\" : true" +
            "}," +
            "{" + "\"firstName\" 	: \"Илья\"," + "\"lastName\"  	: \"Иванов\"," +
                "\"carCategory\" 	: {" + "\"comfort\" : true," + "\"business\" : false," + "\"vip\" : false" + "}," +
                "\"limit\"     	: 2000," + "\"distribution\" : false" +
            "}," +
            "{" + "\"firstName\" 	: \"Семен\"," + "\"lastName\"  	: \"Семенов\"," +
                "\"carCategory\" 	: {" + "\"comfort\" : true," + "\"business\" : true," + "\"vip\" : false" + "}," +
                "\"limit\"     	: 4000," + "\"distribution\" 	: true" +
            "}" +
          "]" +
        "}";

     Budget budget = gson.fromJson(budgetStri, Budget.class);

    return budget;
    }

    public AllowRs getTestCarCategoryAllow() {
        final String allowRsStri = "{" +
                "\"code\" : 0," +
                "\"allowed\" : true" +
                "}";

        AllowRs rs = gson.fromJson(allowRsStri, AllowRs.class);


    return rs;
    }

}
