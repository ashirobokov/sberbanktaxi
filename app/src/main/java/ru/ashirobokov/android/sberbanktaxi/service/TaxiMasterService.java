package ru.ashirobokov.android.sberbanktaxi.service;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import ru.ashirobokov.android.sberbanktaxi.model.CostResult;
import ru.ashirobokov.android.sberbanktaxi.model.OrderResult;
import ru.ashirobokov.android.sberbanktaxi.model.RideTimeResult;

/**
 * Created by AShirobokov on 09.03.2017.
 */
public interface TaxiMasterService {

/*
    Рассчитать время поездки.
*/
    @FormUrlEncoded
    @POST("common_api/1.0/get_ride_time")
    Call<RideTimeResult> getRideTime(@FieldMap Map<String, String> parameters);

/*
    Рассчитать стоимость поездки.
*/
    @GET("common_api/1.0/calc_order_cost")
    Call<CostResult> calcOrderCost(@QueryMap Map<String, String> parameters);

/*
    Выполнить заказ такси.
*/
    @FormUrlEncoded
    @POST("common_api/1.0/create_order")
    Call<OrderResult> createOrder(@FieldMap Map<String, String> parameters);


}
