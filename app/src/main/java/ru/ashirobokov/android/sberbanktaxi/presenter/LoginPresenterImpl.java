package ru.ashirobokov.android.sberbanktaxi.presenter;

import ru.ashirobokov.android.sberbanktaxi.data.LoginHelper;
import ru.ashirobokov.android.sberbanktaxi.model.Authentificate;
import ru.ashirobokov.android.sberbanktaxi.interactor.LoginInteractor;
import ru.ashirobokov.android.sberbanktaxi.interactor.LoginInteractorImpl;
import ru.ashirobokov.android.sberbanktaxi.model.Register;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;
import ru.ashirobokov.android.sberbanktaxi.view.LoginView;

/**
 * Created by AShirobokov on 14.02.2017.
 */
public class LoginPresenterImpl implements LoginPresenter, LoginInteractor.OnRegisterFinishedListener,
                                                                    LoginInteractor.OnLoginFinishedListener{

    private static final String TAG = ConstantManager.TAG_PREFIX + "LoginPresenter";

    private LoginView loginView;
    private LoginInteractor loginInteractor;
    private LoginHelper loginHelper;


    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
        this.loginHelper = new LoginHelper();

    }


    @Override
    public void registerUser(String email, String phone) {
        loginView.showProgress();
        loginInteractor.register(email, phone, this);
    }

    @Override
    public void validateUser(String code) {
        loginView.showProgress();
        loginInteractor.login(code, this);
    }


    @Override
    public void onEmailError() {
        loginView.setEmailError();
        loginView.hideProgress();
    }


    @Override
    public Register getRegister(String email, String phone) {
        return loginHelper.getTestRegister();
    }

    @Override
    public Authentificate getAuthentificate(String code) {
        return loginHelper.getTestAuthetificate();
    }


    @Override
    public void onRegisterError() {
        loginView.showMessage("Код для авторизации не получен");
    }

    @Override
    public void onRegister(Register register) {
        loginView.hideProgress();
        loginView.showLoginViews();
        loginView.setLoginCode(register.getSecurityCode());
    }

    @Override
    public void onLoginCodeError() {
        loginView.setLoginCodeError();
        loginView.hideProgress();
    }

    @Override
    public void onSuccess(Authentificate auth) {
        loginView.navigateToSplash(auth.getId());
    }

    @Override
    public void onDestroy() {
        loginView = null;
    }

}
