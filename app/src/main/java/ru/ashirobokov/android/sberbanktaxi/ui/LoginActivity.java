package ru.ashirobokov.android.sberbanktaxi.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import ru.ashirobokov.android.sberbanktaxi.App;
import ru.ashirobokov.android.sberbanktaxi.R;
import ru.ashirobokov.android.sberbanktaxi.di.components.DaggerLoginComponent;
import ru.ashirobokov.android.sberbanktaxi.di.components.LoginComponent;
import ru.ashirobokov.android.sberbanktaxi.di.modules.LoginModule;
import ru.ashirobokov.android.sberbanktaxi.presenter.LoginPresenter;
import ru.ashirobokov.android.sberbanktaxi.presenter.LoginPresenterImpl;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;
import ru.ashirobokov.android.sberbanktaxi.view.LoginView;

/**
 * Created by ashirobokov on 13.02.2017.
 */
public class LoginActivity extends ProgressActivity implements LoginView {

    private static final String TAG = ConstantManager.TAG_PREFIX + "LoginActivity";

    private TextView userPhone;
    private EditText userEmail;
    private Button regInButton;
    private EditText loginCode;
    private Button loginButton;

    @Inject LoginPresenter mPresenter;
//    private LoginPresenter presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

//        mProgress = (ProgressBar) findViewById(R.id.login_progress);
        userPhone = (TextView) findViewById(R.id.user_phone);
        userEmail = (EditText) findViewById(R.id.user_email);
        regInButton = (Button) findViewById(R.id.reg_in_button);
        loginCode = (EditText) findViewById(R.id.login_code);
        loginButton = (Button) findViewById(R.id.login_button);

        LoginComponent loginComponent = DaggerLoginComponent.builder()
                .loginModule(new LoginModule(this))
                .build();

        loginComponent.inject(this);

//      Теперь presenter инджектится через Dagger2 (LoginComponent)
//       presenter = new LoginPresenterImpl(this);

        regInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.registerUser(userEmail.getText().toString(), userPhone.getText().toString());
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.validateUser(loginCode.getText().toString());
            }
        });

    }

/*

    private void loginViewsDisabled() {
        loginCode.setEnabled(false);
        loginButton.setEnabled(false);
    }

*/

/*
    @Override
    public void showProgress() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgress.setVisibility(View.GONE);
    }
*/

    @Override
    public void setEmailError() {
        userEmail.setError("Некорректный Емайл Адрес");
    }

    @Override
    public void showLoginViews() {
        loginCode.setVisibility(View.VISIBLE);
        loginButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLoginCodeError() {
        loginCode.setError("Некорректный Код");
    }

    @Override
    public void setLoginCode(String code) {
        loginCode.setText(code);
    }

    @Override
    public void navigateToSplash(int id) {
        Intent splash = new Intent(this, SplashActivity.class);
        splash.putExtra("id", id);
        startActivity(splash);
        finish();

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

    }

}
