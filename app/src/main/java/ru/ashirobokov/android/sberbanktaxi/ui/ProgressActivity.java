package ru.ashirobokov.android.sberbanktaxi.ui;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import ru.ashirobokov.android.sberbanktaxi.R;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;

/**
 * Created by Shirobokov on 18.02.2017.
 */

public class ProgressActivity extends AppCompatActivity {

    private static final String TAG = ConstantManager.TAG_PREFIX + "ProgressActivity";
    protected ProgressDialog mProgressDialog;

    public void showProgress() {


        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        }

        mProgressDialog.show();

    }

    public void hideProgress() {
        if (mProgressDialog != null)
            if (mProgressDialog.isShowing())
                mProgressDialog.hide();
    }

}


