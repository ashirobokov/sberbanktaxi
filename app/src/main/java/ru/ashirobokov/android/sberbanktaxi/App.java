package ru.ashirobokov.android.sberbanktaxi;

import android.app.Application;
import android.content.Context;

import ru.ashirobokov.android.sberbanktaxi.di.components.AppComponent;
import ru.ashirobokov.android.sberbanktaxi.di.components.DaggerAppComponent;
import ru.ashirobokov.android.sberbanktaxi.di.modules.AppModule;

/**
 * Created by AShirobokov on 27.02.2017.
 */
public class App extends Application {

    private AppComponent mComponent;
    private Context mContext;
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = createComponent();
        app = this;
        mContext = app.getApplicationContext();

    }

    public AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    public AppComponent getComponent() {
        return mComponent;
    }

    public Context getContext() {
        return mContext;
    }

    public static App get() {
        return app;
    }

}
