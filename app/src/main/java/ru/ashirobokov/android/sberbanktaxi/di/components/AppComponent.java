package ru.ashirobokov.android.sberbanktaxi.di.components;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import ru.ashirobokov.android.sberbanktaxi.di.modules.AppModule;
import ru.ashirobokov.android.sberbanktaxi.service.SberTaxiService;

/**
 * Created by AShirobokov on 27.02.2017.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    Gson gson();
    SberTaxiService sberTaxiService();

}
