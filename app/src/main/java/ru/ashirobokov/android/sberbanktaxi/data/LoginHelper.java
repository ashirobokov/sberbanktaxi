package ru.ashirobokov.android.sberbanktaxi.data;

import android.util.Log;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.ashirobokov.android.sberbanktaxi.App;
import ru.ashirobokov.android.sberbanktaxi.model.Authentificate;
import ru.ashirobokov.android.sberbanktaxi.model.Register;
import ru.ashirobokov.android.sberbanktaxi.service.SberTaxiService;
import ru.ashirobokov.android.sberbanktaxi.utils.ConstantManager;

/**
 * Created by AShirobokov on 01.03.2017.
 */
public class LoginHelper {

    private static final String TAG = ConstantManager.TAG_PREFIX + "LoginHelper";

    private Gson gson;
    private SberTaxiService service;

    public LoginHelper() {

        gson = App.get().getComponent().gson();
        service = App.get().getComponent().sberTaxiService();

    }

    //region ===================== Test methods ==============================

    public Register getTestRegister() {
        final String rspRegister = "{\"securityCode\":\"1234\"}";

        Register register = gson.fromJson(rspRegister, Register.class);

        return register;
    }


    public Authentificate getTestAuthetificate() {
        final String rspAuth = "{" +
                "\"resultCode\" : 0,\n" +
                "\"id\" : 856340123\n" +
                "}";

        Authentificate auth = gson.fromJson(rspAuth, Authentificate.class);

        return auth;
    }
    //endregion


    //region ==================== Network methods ==============================

    public Register getNetRegister(String email, String phone) {
        final Register register = new Register();

        Call<Register> call = service.getRegistration(email, phone);

        call.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                if (response.code() == 200) {

                    try {

                        register.setSecurityCode(response.body().getSecurityCode());

                    } catch (NullPointerException e) {
                        Log.d(TAG, e.toString());
                    }

                } else if (response.code() == 404) {
                    Log.d(TAG, "Неверный емайл или номер телефона");
                } else {
                    Log.d(TAG, "Другая ошибка");
                    if (ConstantManager.DEBUG) {
                        Log.d(TAG, String.valueOf(response.code()));
                    }

                }

            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                Log.d(TAG, t.toString());

            }
        });

        return register;
    }

    public Authentificate getNetAuthetificate(String code) {

        final Authentificate authentificate = new Authentificate();

        Call<Authentificate> call = service.getAuthentification(code);

        call.enqueue(new Callback<Authentificate>() {
            @Override
            public void onResponse(Call<Authentificate> call, Response<Authentificate> response) {
                if (response.code() == 200) {

                    try {

                        authentificate.setResultCode(response.body().getResultCode());
                        authentificate.setId(response.body().getId());

                    } catch (NullPointerException e) {
                        Log.d(TAG, e.toString());
                    }

                } else if (response.code() == 404) {
                    Log.d(TAG, "Неверный безпасный код");
                } else {
                    Log.d(TAG, "Другая ошибка");
                    if (ConstantManager.DEBUG) {
                        Log.d(TAG, String.valueOf(response.code()));
                    }

                }

            }

            @Override
            public void onFailure(Call<Authentificate> call, Throwable t) {
                Log.d(TAG, t.toString());

            }
        });


        return authentificate;
    }
    //endregion


}
