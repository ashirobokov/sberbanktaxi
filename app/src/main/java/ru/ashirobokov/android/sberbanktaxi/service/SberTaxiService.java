package ru.ashirobokov.android.sberbanktaxi.service;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.ashirobokov.android.sberbanktaxi.model.Authentificate;
import ru.ashirobokov.android.sberbanktaxi.model.Register;

/**
 * Created by 1 on 15.02.2017.
 */
public interface SberTaxiService {

    final String SBERTAXI_SERVICE = "http://localhost:9080/sbrf-taxi-service/";

//  Полный запрос к сервису
//  http://localhost:9080/sbrf-taxi-service/registrate/89251231214/aaw.sbt@sberbank.ru

    @GET("registrate/{phone}/{email}")
    Call<Register> getRegistration(@Path("phone") String phone, @Path("email") String email);

//  http://localhost:9080/sbrf-taxi-service/auth/1234

    @GET("auth/{code}")
    Call<Authentificate> getAuthentification(@Path("code") String code);

}