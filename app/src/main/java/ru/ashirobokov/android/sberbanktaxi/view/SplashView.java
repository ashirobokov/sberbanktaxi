package ru.ashirobokov.android.sberbanktaxi.view;

/**
 * Created by AShirobokov on 02.03.2017.
 */
public interface SplashView {

    void showProgress();

    void hideProgress();

    void setHello();

    void showError();

    void navigateToMain();

    void showMessage(String message);

}
