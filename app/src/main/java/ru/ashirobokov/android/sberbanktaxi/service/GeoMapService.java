package ru.ashirobokov.android.sberbanktaxi.service;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.ashirobokov.android.sberbanktaxi.model.Coordinates;

/**
 * Created by 1 on 07.03.2017.
 */
public interface GeoMapService {

//     Полный запрос к сервису
//     Request (https://localhost:9080/geo-map-service/common_api/1.0/convert) POST
//     Body                         (address="ул. Судакова, д.10, корп. 1")

    @FormUrlEncoded
    @POST("common_api/1.0/convert")
    Call<Coordinates> addressConvert(@Field(value="address", encoded = true) String address);

}
