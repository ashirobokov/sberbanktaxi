package ru.ashirobokov.android.sberbanktaxi.di.modules;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import ru.ashirobokov.android.sberbanktaxi.di.anotations.PerActivity;
import ru.ashirobokov.android.sberbanktaxi.presenter.LoginPresenter;
import ru.ashirobokov.android.sberbanktaxi.presenter.LoginPresenterImpl;
import ru.ashirobokov.android.sberbanktaxi.service.SberTaxiService;
import ru.ashirobokov.android.sberbanktaxi.ui.LoginActivity;
import ru.ashirobokov.android.sberbanktaxi.view.LoginView;

/**
 * Created by 1 on 27.02.2017.
 */
@Module
public class LoginModule {

    private LoginActivity activity;

    public LoginModule(LoginActivity activity) {
            this.activity = activity;
    }

    @Provides
    @PerActivity
    LoginView provideLoginView() {
        return activity;
    }

    @Provides
    @PerActivity
    LoginPresenter provideLoginPresenter(LoginView view) {
        return new LoginPresenterImpl(view);
    }

}
