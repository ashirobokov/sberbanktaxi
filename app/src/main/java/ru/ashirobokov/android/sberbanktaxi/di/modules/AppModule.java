package ru.ashirobokov.android.sberbanktaxi.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.ashirobokov.android.sberbanktaxi.service.NetworkRxServiceGenerator;
import ru.ashirobokov.android.sberbanktaxi.service.NetworkServiceGenerator;
import ru.ashirobokov.android.sberbanktaxi.service.SberRxTaxiService;
import ru.ashirobokov.android.sberbanktaxi.service.SberTaxiService;

/**
 * Created by AShirobokov on 27.02.2017.
 */

@Module
public class AppModule {

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    @Singleton
    SberTaxiService provideSberTaxiService() {
        return NetworkServiceGenerator.createNetworkService(SberTaxiService.SBERTAXI_SERVICE, SberTaxiService.class);
    }

    @Provides
    @Singleton
    SberRxTaxiService provideSberRxTaxiService() {
        return NetworkRxServiceGenerator.createNetworkRxService(SberRxTaxiService.SBERTAXI_SERVICE, SberRxTaxiService.class);
    }

}
