package ru.ashirobokov.android.sberbanktaxi.presenter;

/**
 * Created by AShirobokov on 21.03.2017.
 */
public interface MainPresenter {

    /**
     *  1.Заполнить форму
     *   1.1. Откуда /. online address like запрос ./ (текущее местоположение)
     *   1.2. Куда /. online address like запрос ./ (адрес)
     *   1.3. Отобразить на карте
     *   1.4. Заполнить время ПОДАЧИ и время ПРИБЫТИЯ
     *   1.6. Нажать кнопку категории сервиса
     *   1.7. Получение разрешения /. block кнопки Заказать на время запроса ./ если категория не предоставлена
     *   1.8. Выполнить Заказ
     *          - сообщение "заказ обрабатывается таксопарком"
     *          - approved "Детали Заказа"
     *
     */

    int getLimit();

    void showViewsInitial();

    boolean setupRequestCategory(String category);

    void requestForCarCategory();

    void createOrder();

    void onDestroy();

}
