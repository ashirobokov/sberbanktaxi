package ru.ashirobokov.android.sberbanktaxi.view;

/**
 * Created by 1 on 13.02.2017.
 */
public interface LoginView {

    void showProgress();

    void hideProgress();

    void setEmailError();

    void showLoginViews();

    void setLoginCodeError();

    void setLoginCode(String code);

    void navigateToSplash(int id);

    void showMessage(String message);

}
