package ru.ashirobokov.android.sberbanktaxi.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ru.ashirobokov.android.sberbanktaxi.App;
import ru.ashirobokov.android.sberbanktaxi.R;
import ru.ashirobokov.android.sberbanktaxi.view.SplashView;

/**
 * Created by AShirobokov on 02.03.2017.
 */
public class SplashActivity extends ProgressActivity implements SplashView {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);

        navigateToMain();

    }

    @Override
    public void showProgress() {
        super.showProgress();
    }


    @Override
    public void hideProgress() {
        super.hideProgress();
    }


    @Override
    public void setHello() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showMessage(String message) {


    }


    @Override
    public void navigateToMain() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent main = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(main);
                finish();
            }
        }, 3000);

    }

}
