package ru.ashirobokov.android.sberbanktaxi.presenter;

/**
 * Created by 1 on 14.02.2017.
 */
public interface LoginPresenter {

    void registerUser(String email, String phone);

    void validateUser(String code);

    void onDestroy();

}
