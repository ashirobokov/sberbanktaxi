package ru.ashirobokov.android.sberbanktaxi.di.anotations;

import java.lang.annotation.Retention;

import javax.inject.Scope;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by 1 on 27.02.2017.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}
