package ru.ashirobokov.android.sberbanktaxi.interactor;

import ru.ashirobokov.android.sberbanktaxi.model.Authentificate;
import ru.ashirobokov.android.sberbanktaxi.model.Register;

/**
 * Created by ashirobokov on 14.02.2017.
 */
public interface LoginInteractor {

    interface OnRegisterFinishedListener {
        void onEmailError();

        Register getRegister(String email, String phone);

        void onRegisterError();

        void onRegister(Register register);
    }

    interface OnLoginFinishedListener {

            void onLoginCodeError();

            Authentificate getAuthentificate(String code);

            void onSuccess(Authentificate auth);

    }

    void register(String email, String phone, OnRegisterFinishedListener listener);

    void login(String code, OnLoginFinishedListener listener);

}
