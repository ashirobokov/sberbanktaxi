package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Shirobokov on 15.02.2017.
 */
public class Authentificate {


    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("resultCode")
    @Expose
    private int resultCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public String toString() {
        return "Authentificate{" +
                "id=" + id +
                ", resultCode=" + resultCode +
                '}';
    }

}

