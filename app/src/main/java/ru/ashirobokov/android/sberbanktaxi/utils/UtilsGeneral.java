package ru.ashirobokov.android.sberbanktaxi.utils;

import android.app.Activity;
import android.widget.Toast;

import ru.ashirobokov.android.sberbanktaxi.App;

/**
 * Created by AShirobokov on 16.02.2017.
 */
public class UtilsGeneral {

    public static void showMessage(String message) {
        Toast.makeText(App.get().getContext(), message, Toast.LENGTH_LONG).show();
    }

}
