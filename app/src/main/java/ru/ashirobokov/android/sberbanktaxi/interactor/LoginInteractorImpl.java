package ru.ashirobokov.android.sberbanktaxi.interactor;

import android.os.Handler;

import ru.ashirobokov.android.sberbanktaxi.model.Authentificate;
import ru.ashirobokov.android.sberbanktaxi.model.Register;

/**
 * Created by Shirobokov on 15.02.2017.
 */
public class LoginInteractorImpl implements LoginInteractor {

    Register register;
    Authentificate auth;

    @Override
    public void register(final String email, final String phone, final OnRegisterFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;

                if(!email.contains("@")) {
                   listener.onEmailError();
                    error = true;
                }
                if ((register = listener.getRegister(email, phone)) == null) {
                    listener.onRegisterError();
                    error = true;
                }

                if (!error){
                    listener.onRegister(register);
                }
            }
        }, 2000);

    }


    @Override
    public void login(final String code, final OnLoginFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;

                    if (code.length() < 4){
                        listener.onLoginCodeError();
                        error = true;
                        return;
                    }

                    auth = listener.getAuthentificate(code);
                    if ((auth.getResultCode()) != 0) {
                            error = true;
                    }


                    if (!error) {
                        listener.onSuccess(auth);
                    }

                }

        }, 2000);

    }


}
