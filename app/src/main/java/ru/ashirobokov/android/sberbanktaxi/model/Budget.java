package ru.ashirobokov.android.sberbanktaxi.model;

/**
 * Created by Shirobokov on 05.03.2017.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Budget {


    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("assignedLimit")
    @Expose
    private Integer assignedLimit;
    @SerializedName("distributedLimit")
    @Expose
    private Integer distributedLimit;
    @SerializedName("restOfLimit")
    @Expose
    private Integer restOfLimit;
    @SerializedName("myCarCategory")
    @Expose
    private CarCategory myCarCategory;
    @SerializedName("colleagues")
    @Expose
    private List<Colleague> colleagues = null;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAssignedLimit() {
        return assignedLimit;
    }

    public void setAssignedLimit(Integer assignedLimit) {
        this.assignedLimit = assignedLimit;
    }

    public Integer getDistributedLimit() {
        return distributedLimit;
    }

    public void setDistributedLimit(Integer distributedLimit) {
        this.distributedLimit = distributedLimit;
    }

    public Integer getRestOfLimit() {
        return restOfLimit;
    }

    public void setRestOfLimit(Integer restOfLimit) {
        this.restOfLimit = restOfLimit;
    }

    public CarCategory getMyCarCategory() {
        return myCarCategory;
    }

    public void setMyCarCategory(CarCategory myCarCategory) {
        this.myCarCategory = myCarCategory;
    }

    public List<Colleague> getColleagues() {
        return colleagues;
    }

    public void setColleagues(List<Colleague> colleagues) {
        this.colleagues = colleagues;
    }

    @Override
    public String toString() {
        return "Budget{" +
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", assignedLimit=" + assignedLimit +
                ", distributedLimit=" + distributedLimit +
                ", restOfLimit=" + restOfLimit +
                ", myCarCategory=" + myCarCategory +
                ", colleagues=" + colleagues +
                '}';
    }

    public static class Colleague {

        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("carCategory")
        @Expose
        private CarCategory carCategory;
        @SerializedName("limit")
        @Expose
        private Integer limit;
        @SerializedName("distribution")
        @Expose
        private Boolean distribution;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public CarCategory getCarCategory() {
            return carCategory;
        }

        public void setCarCategory(CarCategory carCategory) {
            this.carCategory = carCategory;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }

        public Boolean getDistribution() {
            return distribution;
        }

        public void setDistribution(Boolean distribution) {
            this.distribution = distribution;
        }

        @Override
        public String toString() {
            return "Colleague{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", carCategory=" + carCategory +
                    ", limit=" + limit +
                    ", distribution=" + distribution +
                    '}';
        }

    }


    public static class CarCategory {

        @SerializedName("comfort")
        @Expose
        private Boolean comfort;
        @SerializedName("business")
        @Expose
        private Boolean business;
        @SerializedName("vip")
        @Expose
        private Boolean vip;

        public Boolean getComfort() {
            return comfort;
        }

        public void setComfort(Boolean comfort) {
            this.comfort = comfort;
        }

        public Boolean getBusiness() {
            return business;
        }

        public void setBusiness(Boolean business) {
            this.business = business;
        }

        public Boolean getVip() {
            return vip;
        }

        public void setVip(Boolean vip) {
            this.vip = vip;
        }


        @Override
        public String toString() {
            return "CarCategory{" +
                    "comfort=" + comfort +
                    ", business=" + business +
                    ", vip=" + vip +
                    '}';
        }

    }

}
