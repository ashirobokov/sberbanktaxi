package ru.ashirobokov.android.sberbanktaxi.view;

import java.util.List;

/**
 * Created by AShirobokov on 21.03.2017.
 */
public interface MainView {

    void showProgress();

    void hideProgress();

    void showInitialMapLocation();

    void showToggleButtonChecked(String category);

    void showOrderButtonActive(boolean enabled);

    void showMessage(String message);

}
