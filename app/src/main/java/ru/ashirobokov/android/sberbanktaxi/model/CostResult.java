package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AShirobokov on 09.03.2017.
 */
public class CostResult {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("descr")
    @Expose
    private String descr;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CostResult{" +
                "code=" + code +
                ", descr='" + descr + '\'' +
                ", data=" + data +
                '}';
    }

        public static class Data {

            @SerializedName("sum")
            @Expose
            private Integer sum;
            @SerializedName("info")
            @Expose
            private List<Info> info = null;

            public Integer getSum() {
                return sum;
            }

            public void setSum(Integer sum) {
                this.sum = sum;
            }

            public List<Info> getInfo() {
                return info;
            }

            public void setInfo(List<Info> info) {
                this.info = info;
            }


            @Override
            public String toString() {
                return "Data{" +
                        "sum=" + sum +
                        ", info=" + info +
                        '}';
            }
        }

        public static class Info {

            @SerializedName("comment")
            @Expose
            private String comment;
            @SerializedName("sum")
            @Expose
            private String sum;

            public String getComment() {
                return comment;
            }

            public void setComment(String comment) {
                this.comment = comment;
            }

            public String getSum() {
                return sum;
            }

            public void setSum(String sum) {
                this.sum = sum;
            }

            @Override
            public String toString() {
                return "Info{" +
                        "comment='" + comment + '\'' +
                        ", sum='" + sum + '\'' +
                        '}';
            }

        }

}
