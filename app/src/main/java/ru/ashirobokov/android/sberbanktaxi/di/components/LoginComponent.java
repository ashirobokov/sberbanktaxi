package ru.ashirobokov.android.sberbanktaxi.di.components;

import dagger.Component;
import ru.ashirobokov.android.sberbanktaxi.di.anotations.PerActivity;
import ru.ashirobokov.android.sberbanktaxi.di.modules.LoginModule;
import ru.ashirobokov.android.sberbanktaxi.ui.LoginActivity;

/**
 * Created by 1 on 27.02.2017.
 */
@PerActivity
@Component(modules = LoginModule.class)
public interface LoginComponent {

    void inject(LoginActivity activity);

}
