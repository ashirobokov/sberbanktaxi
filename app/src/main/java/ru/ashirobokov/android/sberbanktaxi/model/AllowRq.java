package ru.ashirobokov.android.sberbanktaxi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AShirovokov on 16.03.2017.
 */
public class AllowRq {

    @SerializedName("carCategory")
    @Expose
    private String carCategory;

    public String getCarCategory() {
        return carCategory;
    }

    public void setCarCategory(String carCategory) {
        this.carCategory = carCategory;
    }

    @Override
    public String toString() {
        return "AllowRq{" +
                "carCategory='" + carCategory + '\'' +
                '}';
    }

}
